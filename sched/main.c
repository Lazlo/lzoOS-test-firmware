/* Written for Rainbowduino (ATmega328p at 16MHz) */

#define BAUD 38400
#define TICKS_PER_SEC 10000

#include "USART.h"
#include "Timer.h"
#include "Sched.h"
#include "Uptime.h"
#include <avr/io.h>
#include <avr/interrupt.h>
#include <util/delay.h>

static USART uart;
static Timer t0;
static uint8_t t0_reload;

static void uart_init(const uint32_t baud)
{
	uart = USART_Create(&UCSR0A, &UCSR0B, &UCSR0C, &UBRR0L, &UBRR0H, &UDR0);
	USART_SetBaudRate(uart, baud, F_CPU);
	USART_Enable(uart, 1);
	USART_Puts(uart, "USART\r\n");
}

static void timer_init(const uint32_t overflowPerSecond)
{
	const uint16_t ps = 8;
	t0_reload = 255 - (F_CPU / ps / overflowPerSecond);
	t0 = Timer_Create(1, 0, &TCCR0A, &TCCR0B,
				&TCNT0, 0,
				&OCR0A, 0,
				&OCR0B, 0,
				0, 0,
				&TIMSK0, &TIFR0);
	Timer_SetOverflowInterrupt(t0, 1);
	Timer_SetCounter(t0, t0_reload);
	Timer_SetPrescaler(t0, ps);
	USART_Puts(uart, "Timer0\r\n");
}

static void testTask(void)
{
	char utstr[9];
	Uptime_Tick();
	Uptime_ToStr(utstr);
	USART_Puts(uart, utstr);
	USART_Puts(uart, " tick\r\n");
}

static void sched_init(void)
{
	Sched_Create();
	Sched_AddTask(testTask, TICKS_PER_SEC, 0);
	USART_Puts(uart, "Sched\r\n");
}

int main(void)
{
	uart_init(BAUD);
	timer_init(TICKS_PER_SEC);
	Uptime_Create();
	sched_init();
	sei();
	USART_Puts(uart, "IRQ\r\n");
	while (1)
		Sched_Dispatch();
}

ISR(TIMER0_OVF_vect)
{
	Timer_SetCounter(t0, t0_reload);
	Sched_Update();
}
